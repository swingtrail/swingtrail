# Swing Trail
We are Swing Trail Trader who develop an indicator script to help everyone get efficient profit.

---
## 1. Swing Trail Signal PRO
Let's do a Swing Trading like a PRO.

![](https://i.imgur.com/jiO4etD.png)

### Solutions :
- Trading with confidence to Forex, Stocks and Crypto, support all timeframes.
- Buy and Sell Signal, You don't rely on group VIP signal.
- Auto analyst the trend market and pricing target, don't waste your time to draw the lines.
- Secret strategy to achieve 90% WIN rate and get efficient profit.
- Alerts to inform you the golden moment or use webhook for playing with your BOT trading application.
- Simple, only one indicator contains multiple indicators.

### Documentation
- Please see our [Wiki](https://gitlab.com/swingtrail/swingtrail/-/wikis/home).

### Free Version
You can get the Swing Trail Signal Free Version at here >> [https://www.tradingview.com/script/oBrsXwvb-aalfiann-Swing-Trail-Signal/](https://www.tradingview.com/script/oBrsXwvb-aalfiann-Swing-Trail-Signal/)

---
Author: M ABD AZIZ ALFIAN  
Twitter: [@aalfiann](https://twitter.com/aalfiann)
